import express from 'express';
const router = express.Router();

import productRoute from './product.route';
import orderRoute from './order.route';
import userRoute from './user.route';
/**
 * Function contains Application routes
 *
 * @returns router
 */
const routes = () => {
  router.get('/', (req, res) => {
    res.json('Welcome');
  });
  router.use('/users', userRoute);

  router.use('/orders', orderRoute);
  router.use('/products', productRoute);
  return router;
};

export default routes;
