import { Schema, model } from 'mongoose';

const orderProductSchema = new Schema(
  {
    order: {
      type: Schema.Types.ObjectId,
      ref: 'Order'
    },
    product: {
      type: Schema.Types.ObjectId,
      ref: 'Product'
    },
    count: {
      type: Number
    },
    price: {
      type: String
    },
    totalPrice: {
      type: String
    }
  },
  {
    timestamps: true
  }
);

export default model('OrderProduct', orderProductSchema);
