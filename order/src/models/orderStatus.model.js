import { Schema, model } from 'mongoose';

const orderStatusSchema = new Schema(
  {
    order: {
      type: Schema.Types.ObjectId,
      ref: 'Order'
    },
    status: {
      type: Schema.Types.ObjectId,
      ref: 'Status'
    },
    isActive: {
      type: Boolean,
      default: 0
    }
  },
  {
    timestamps: true
  }
);

export default model('OrderStatus', orderStatusSchema);
