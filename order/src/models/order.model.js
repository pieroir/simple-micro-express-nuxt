import { Schema, model } from 'mongoose';

const orderSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    payReference: {
      type: String
    },
    payStatus: {
      type: String
    },
    comment: {
      type: String
    },
    orderProducts: [
      {
        type: Schema.Types.ObjectId,
        ref: 'OrderProduct'
      }
    ],
    orderStatuses: [
      {
        type: Schema.Types.ObjectId,
        ref: 'OrderStatus'
      }
    ]
  },

  {
    timestamps: true
  }
);

export default model('Order', orderSchema);
