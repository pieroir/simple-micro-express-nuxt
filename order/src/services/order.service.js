import dotenv from 'dotenv';
import fetch from 'node-fetch';
import Order from '../models/order.model';
import OrderProduct from '../models/orderProduct.model';
import OrderStatus from '../models/orderStatus.model';
import Status from '../models/status.model';
import Product from '../models/product.model';
import User from '../models/user.model';

dotenv.config();
//get all orders
export const getAllOrders = async (userId = null) => {
  let data = await Order.find({
    user: userId
  });

  return data;
};

//create new order
export const newOrder = async (body) => {
  const data = await Order.create(body);

  data.user = await User.findById(body.user);
  await data.save();
  body.products.forEach(async (index) => {
    let product = await Product.findById(index._id);
    let orderProduct = await OrderProduct.create({
      count: index.count,
      price: index.price,
      totalPrice: index.price * index.count
    });
    orderProduct.Order = data;
    orderProduct.Product = product;
    await orderProduct.save();

    data.orderProducts.push(orderProduct);
  });
  await data.save();

  let createdStatus = await Status.findOne({ name: 'created' });
  let orderStatus = await OrderStatus.create({
    isActive: 1
  });
  orderStatus.Order = data;
  orderStatus.Status = createdStatus;
  await orderStatus.save();
  data.orderStatuses.push(orderStatus);
  //payment
  let post = {
    order: data._id,
    user: data.user.email
  };
  await fetch(process.env.PAYMENT_URL, {
    method: 'POST',
    body: JSON.stringify(post),
    headers: { 'Content-Type': 'application/json' }
  })
    .then((res) => res.json())
    .then((json) => {
      if (json.code === 201) {
        let reference = json.data.reference;
        let status = json.data.status;
        data.payReference = reference;
        data.payStatus = status;
      } else {
        data.payStatus = 'failed server';
      }
    });
  await data.save();

  return data;
};

//update single order
export const updateOrder = async (_id, body) => {
  const data = await Order.findByIdAndUpdate(
    {
      _id
    },
    body,
    {
      new: true
    }
  );
  return data;
};

//delete single order
export const deleteOrder = async (id) => {
  await Order.findByIdAndDelete(id);
  return '';
};

//get single order
export const getOrder = async (id) => {
  const data = await Order.findById(id);
  return data;
};
