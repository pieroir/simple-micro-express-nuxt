import Product from '../models/product.model';

//get all products
export const getAllProducts = async () => {
  const data = await Product.find();
  return data;
};

//create new product
export const newProduct = async (body) => {
  const data = await Product.create(body);
  return data;
};

//update single product
export const updateProduct = async (_id, body) => {
  const data = await Product.findByIdAndUpdate(
    {
      _id
    },
    body,
    {
      new: true
    }
  );
  return data;
};

//delete single product
export const deleteProduct = async (id) => {
  await Product.findByIdAndDelete(id);
  return '';
};

//get single product
export const getProduct = async (id) => {
  const data = await Product.findById(id);
  return data;
};
