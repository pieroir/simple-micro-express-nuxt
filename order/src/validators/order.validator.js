import Joi from '@hapi/joi';

export const newOrderValidator = (req, res, next) => {
  const schema = Joi.object({
    comment: Joi.string().min(4),
    products: Joi.array()
  });
  const { error, value } = schema.validate(req.body);
  if (error) {
    next(error);
  } else {
    req.validatedBody = value;
    next();
  }
};
