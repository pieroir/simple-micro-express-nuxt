import seeder from 'mongoose-seed';

seeder.connect('mongodb://localhost:27017/tractive', function () {
  seeder.loadModels([
    'src/models/user.model.js',
    'src/models/status.model.js',
    'src/models/product.model.js'
  ]);
  seeder.clearModels(['User', 'Status', 'Product'], function () {
    seeder.populateModels(data, function () {
      seeder.disconnect();
    });
  });
});
var data = [
  {
    model: 'User',
    documents: [
      {
        fname: 'demo',
        email: 'demo@demo.com',
        password: 'fe01ce2a7fbac8fafaed7c982a04e229' //demo
      }
    ]
  },
  {
    model: 'Status',
    documents: [
      { name: 'created' },
      { name: 'confirmed' },
      { name: 'delivered' },
      { name: 'cancelled' }
    ]
  },
  {
    model: 'Product',
    documents: [
      {
        name: 'Vue js',
        isActive: true,
        body:
          // eslint-disable-next-line max-len
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries'
      },
      {
        name: 'Nuxt js',
        isActive: true,
        body:
          // eslint-disable-next-line max-len
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries'
      },
      {
        name: 'Express js',
        isActive: true,
        body:
          // eslint-disable-next-line max-len
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries'
      },
      {
        name: 'Vuetify',
        isActive: true,
        body:
          // eslint-disable-next-line max-len
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries'
      }
    ]
  }
];
