import Payment from '../models/payment.model';

//get all payments
export const getAllPayments = async () => {
  const data = await Payment.find();
  return data;
};

//create new payment
export const newPayment = async (body) => {
  body.status = Math.floor(Math.random() * 10) > 5 ? 'success' : 'failed';
  body.reference = Math.floor(Math.random() * 9999999999);
  const data = await Payment.create(body);
  return data;
};

//update single payment
export const updatePayment = async (_id, body) => {
  const data = await Payment.findByIdAndUpdate(
    {
      _id
    },
    body,
    {
      new: true
    }
  );
  return data;
};

//delete single payment
export const deletePayment = async (id) => {
  await Payment.findByIdAndDelete(id);
  return '';
};

//get single payment
export const getPayment = async (id) => {
  const data = await Payment.findById(id);
  return data;
};
