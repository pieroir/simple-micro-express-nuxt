import { Schema, model } from 'mongoose';

const paymentSchema = new Schema(
  {
    order: {
      type: String
    },
    user: {
      type: String
    },
    status: {
      type: String
    },
    reference: {
      type: String
    }
  },
  {
    timestamps: true
  }
);

export default model('Payment', paymentSchema);
