import dotenv from 'dotenv';
import fetch from 'node-fetch';
import HttpStatus from 'http-status-codes';
import * as PaymentService from '../services/payment.service';

dotenv.config();

/**
 * Controller to get all payments available
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const getAllPayments = async (req, res, next) => {
  try {
    const data = await PaymentService.getAllPayments();
    res.status(HttpStatus.OK).json({
      code: HttpStatus.OK,
      data: data,
      message: 'All payments fetched successfully'
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to get a single payment
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const getPayment = async (req, res, next) => {
  try {
    const data = await PaymentService.getPayment(req.params._id);
    res.status(HttpStatus.OK).json({
      code: HttpStatus.OK,
      data: data,
      message: 'Payment fetched successfully'
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to create a new payment
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const newPayment = async (req, res, next) => {
  try {
    //valid order
    await fetch(process.env.ORDER_URL + req.body.order)
      .then((res) => res.json())
      .then((json) => {
        if (json.code !== 201) {
          res.status(HttpStatus.CREATED).json({
            code: HttpStatus.NOT_FOUND,
            data: null,
            message: 'order not found'
          });
        }
      });
    const data = await PaymentService.newPayment(req.body);
    res.status(HttpStatus.CREATED).json({
      code: HttpStatus.CREATED,
      data: data,
      message: 'Payment created successfully'
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to update a payment
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const updatePayment = async (req, res, next) => {
  try {
    const data = await PaymentService.updatePayment(req.params._id, req.body);
    res.status(HttpStatus.ACCEPTED).json({
      code: HttpStatus.ACCEPTED,
      data: data,
      message: 'Payment updated successfully'
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Controller to delete a payment
 * @param  {object} req - request object
 * @param {object} res - response object
 * @param {Function} next
 */
export const deletePayment = async (req, res, next) => {
  try {
    await PaymentService.deletePayment(req.params._id);
    res.status(HttpStatus.OK).json({
      code: HttpStatus.OK,
      data: [],
      message: 'Payment deleted successfully'
    });
  } catch (error) {
    next(error);
  }
};
