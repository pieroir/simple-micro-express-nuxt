import express from 'express';
const router = express.Router();
import paymentRoute from './payment.route';

/**
 * Function contains Application routes
 *
 * @returns router
 */
const routes = () => {
  router.get('/', (req, res) => {
    res.json('Welcome');
  });
  router.use('/payments', paymentRoute);
  return router;
};

export default routes;
