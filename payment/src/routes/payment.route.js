import express from 'express';
import * as paymentController from '../controllers/payment.controller';
// import { newPaymentValidator } from '../validators/payment.validator';
// import { paymentAuth } from '../middlewares/auth.middleware';

const router = express.Router();

//route to get all payments
router.get('', paymentController.getAllPayments);

//route to create a new payment
router.post('', paymentController.newPayment);

//route to get a single payment by their payment id
router.get('/:_id', paymentController.getPayment);

//route to update a single payment by their payment id
router.put('/:_id', paymentController.updatePayment);

//route to delete a single payment by their payment id
router.delete('/:_id', paymentController.deletePayment);

export default router;
